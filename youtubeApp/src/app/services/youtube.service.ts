import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { YoutubeResponse } from '../models/youtube.models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  // API
  // https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyAxJ_vXZk_qa5X1O7fo0F4wBO0uP2s50sU&part=snippet&playlistId=UUnNxJZitVfjEzVorV3S2vyQ

  apiKey     = 'AIzaSyAxJ_vXZk_qa5X1O7fo0F4wBO0uP2s50sU';
  part       = 'snippet';
  playlistId = 'UUnNxJZitVfjEzVorV3S2vyQ';
  youtubeUrl = 'https://www.googleapis.com/youtube/v3';
  nextPageToken = '';

  constructor(private http: HttpClient) {

  }

  consumer() {

    const url = `${ this.youtubeUrl }/playlistItems`;

    const params = new HttpParams()
          .set('key', this.apiKey )
          .set('playlistId', this.playlistId )
          .set('part', this.part )
          .set('maxResults', '6' )
          .set('pageToken', this.nextPageToken );
  
    // Tipado
    return this.http.get<YoutubeResponse>( url, { params }).pipe (
      map( res => {
        this.nextPageToken = res.nextPageToken;
        return res.items;

      }), map ( items => {
        return items.map( video => video.snippet);
      })
    );
  }




}
