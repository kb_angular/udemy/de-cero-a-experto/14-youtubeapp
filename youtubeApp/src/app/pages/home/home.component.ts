import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';


import { YoutubeService } from '../../services/youtube.service';
import { Video } from 'src/app/models/youtube.models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  videos: Video[] = [];
  constructor(private youtube: YoutubeService) { }


  ngOnInit(): void {
    this.cargarVideos();

  }

  cargarVideos() {
    this.youtube.consumer().subscribe( res => {
      // console.log(res);

      // Cuando se van a agregar mas  elementos
      this.videos.push(...res);

      console.log( this.videos );

    });
  }

  mostrarVideo( video: Video) {
    console.log( video );
    Swal.fire({
      html: `
      <h8>${ video.title }</h8>
      <iframe
          width="100%"
          height="315" 
          src="https://www.youtube.com/embed/${video.resourceId.videoId}"
          frameborder="0"
          allow="accelerometer;
          autoplay;
          encrypted-media;
          gyroscope;
          picture-in-picture"
          allowfullscreen></iframe>
      `
    });
  }

}
